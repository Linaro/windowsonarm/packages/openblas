#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    if [ -d OpenBLAS ]; then
        return
    fi

    git clone https://github.com/xianyi/OpenBLAS.git
    pushd OpenBLAS
    git checkout $version
    git log -n1
    popd
}

build()
{
    pushd OpenBLAS
    mkdir -p build
    mkdir -p out
    cd build
    if [ ! -f configured ]; then
        cmake .. -G "Ninja" \
        -DCMAKE_C_COMPILER=clang-cl \
        -DCMAKE_Fortran_COMPILER=flang-new \
        -DCMAKE_BUILD_TYPE=Release \
        -DBUILD_SHARED_LIBS=TRUE

        # can't be built with static + shared libraries with MSVC
        # https://github.com/xianyi/OpenBLAS/blob/6bc079687fc09546d02e08951494c51baa9a5535/CMakeLists.txt#L56
        touch configured
    fi
    ninja
    # add openblas.dll to path
    export PATH=$(pwd)/lib/:$PATH
    ctest
    cmake --install . --prefix=../out
    rsync -av ../out/ $out_dir/
    popd
}

checkout
build